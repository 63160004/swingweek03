/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing03;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author L4ZY
 */
public class ListExample {

    ListExample() {
        JFrame f = new JFrame();
        final JLabel label = new JLabel();
        label.setSize(500, 100);
        JButton b = new JButton("Show");
        b.setBounds(200, 150, 80, 30);
        final DefaultListModel<String> l1 = new DefaultListModel<>();
        l1.addElement("SE");
        l1.addElement("CS");
        l1.addElement("IT");
        l1.addElement("Ai");
        final JList<String> list1 = new JList<>(l1);
        list1.setBounds(100, 100, 75, 75);
        DefaultListModel<String> l2 = new DefaultListModel<>();
        l2.addElement("BUU");
        l2.addElement("CMU");
        l2.addElement("KKU");
        l2.addElement("BU");
        final JList<String> list2 = new JList<>(l2);
        list2.setBounds(100, 200, 75, 75);
        f.add(list1);
        f.add(list2);
        f.add(b);
        f.add(label);
        f.setSize(450, 450);
        f.setLayout(null);
        f.setVisible(true);
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "";
                if (list1.getSelectedIndex() != -1) {
                    data = "Major Selected: " + list1.getSelectedValue();
                    label.setText(data);
                }
                if (list2.getSelectedIndex() != -1) {
                    data += ", University Selected: ";
                    for (Object frame : list2.getSelectedValues()) {
                        data += frame + " ";
                    }
                }
                label.setText(data);
            }
        });
    }

    public static void main(String args[]) {
        new ListExample();
    }
}
