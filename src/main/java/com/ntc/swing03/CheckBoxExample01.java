/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing03;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author L4ZY
 */
public class CheckBoxExample01 {

    CheckBoxExample01() {
        JFrame f = new JFrame("CheckBoxExample");
        final JLabel l = new JLabel();
        l.setHorizontalAlignment(JLabel.CENTER);
        l.setSize(400, 100);
        JCheckBox checkbox1 = new JCheckBox("Python");
        checkbox1.setBounds(150, 100, 90, 90);
        JCheckBox checkbox2 = new JCheckBox("Java");
        checkbox2.setBounds(150, 150, 90, 90);
        f.add(checkbox1);
        f.add(checkbox2);
        f.add(l);
        checkbox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                l.setText("Python Checkbox: "
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });
        checkbox2.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                l.setText("Java Checkbox: "
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }

        });
        f.setSize(600, 600);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new CheckBoxExample01();
    }
}

