/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing03;

/**
 *
 * @author L4ZY
 */
import javax.swing.*;  
import java.awt.event.*;  
class ScrollBarExample  
{  
ScrollBarExample(){  
    JFrame f= new JFrame("Scrollbar Example");  
    final JLabel label = new JLabel();          
    label.setHorizontalAlignment(JLabel.CENTER);    
    label.setSize(400,100);  
    final JScrollBar s=new JScrollBar();  
    s.setBounds(350,100, 20,250);  
    f.add(s); f.add(label);  
    f.setSize(400,400);  
   f.setLayout(null);  
   f.setVisible(true);  
   s.addAdjustmentListener(new AdjustmentListener() {  
    public void adjustmentValueChanged(AdjustmentEvent e) {  
       label.setText("Vertical Scrollbar value is:"+ s.getValue());  
    }  
 });  
}  
public static void main(String args[])  
{  
   new ScrollBarExample();  
}}  
