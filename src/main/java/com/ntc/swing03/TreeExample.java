/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing03;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author L4ZY
 */
public class TreeExample {

    JFrame f;

    TreeExample() {
        f = new JFrame();
        DefaultMutableTreeNode Faculty = new DefaultMutableTreeNode("Faculty");
        DefaultMutableTreeNode informatics = new DefaultMutableTreeNode("informatics");
        DefaultMutableTreeNode science = new DefaultMutableTreeNode("science");
        Faculty.add(informatics);
        Faculty.add(science);
        DefaultMutableTreeNode CS = new DefaultMutableTreeNode("CS");
        DefaultMutableTreeNode SE = new DefaultMutableTreeNode("SE");
        DefaultMutableTreeNode AI = new DefaultMutableTreeNode("AI");
        DefaultMutableTreeNode IT = new DefaultMutableTreeNode("IT");
        informatics.add(CS);
        informatics.add(SE);
        informatics.add(AI);
        informatics.add(IT);
        JTree jt = new JTree(Faculty);
        f.add(jt);
        f.setSize(200, 200);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new TreeExample();
    }
}
